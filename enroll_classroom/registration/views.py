from django.contrib.auth.models import User
from django.shortcuts import redirect, render
from django.contrib import messages
from django.contrib import auth
from django.views import View
from classroom.models import Classroom
from django.contrib.auth.decorators import login_required

# Create your views here.
class RegistrationView(View):
    def get(self,request):
        return render(request,'registration/register.html')

    def post(self,request):
        #GET USER DATA
        #VALIDATE
        #CREATE USER ACCOUNT

        username = request.POST['username']
        email = request.POST['email']
        password = request.POST['password']   
        context ={
            'fieldValues' : request.POST
        }

        
        if not User.objects.filter(email = 'email').exists():
            if len(password)<6 :
                messages.error(request,'password is too short')
                return render(request,'registration/register.html',context )

            user = User.objects.create_user(username= username, email = email)
            user.set_password(password)
            user.is_active = True
            user.save()
            messages.success(request , "account created successfully")
            return render(request, 'registration/login.html')

class LoginView(View):
    def get(self, request):
        return render(request,'registration/login.html')

    def post(self,request):
        username = request.POST['username']
        password = request.POST['password']
        
        if username and password:
            user = auth.authenticate(username = username , password = password)
            if user:
                if user.is_active:
                    auth.login(request ,user)
                    messages.success(request,'Welcome,'+user.username+'You are now logged in')
                    return redirect('home')
           
            messages.error(request, 'invalid credintial please try again')
            return render(request,'registration/login.html')

        messages.error(request, 'Please fill all fields')
        return render(request,'registration/login.html')


class LogoutView(View):
    def post(self , request):
        auth.logout(request)
        messages.success(request, 'You have been logged out')
        return redirect('login')

@login_required
def home(request):
    classroom_details = Classroom.objects.all()
    context ={
        'classroom_details':classroom_details,
    }
    print(context['classroom_details'])
    if request.method == 'GET':
        return render(request,'home.html', context)

    