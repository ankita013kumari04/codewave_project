from django.urls import path
from . import views
from django.views.decorators.csrf import csrf_exempt
from .views import LoginView, RegistrationView

urlpatterns = [
    path('register/',csrf_exempt(RegistrationView.as_view()),name ="register"),
    path('login/',csrf_exempt(LoginView.as_view()),name ="login"),
    path('home/',views.home,name ="home"),
    
 
]
