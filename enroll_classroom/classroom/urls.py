from django.urls import path
from . import views
from django.views.decorators.csrf import csrf_exempt


urlpatterns = [
    path('<int:id>/',views.class_details,name ="class"),
    path('add_classroom',csrf_exempt(views.add_classroom),name ="add_classroom"),
    path('classroom_details',csrf_exempt(views.classroom_details),name ="classroom_details"),
]
