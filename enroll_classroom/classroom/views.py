from django.shortcuts import redirect, render
from .models import Classroom, ClassRegister
from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required
def class_details(request,id):
    classroom_details = Classroom.objects.get(id = id)
    context ={
        'classroom_details':classroom_details,
    }
    if request.method == 'GET':
        return render(request,'classroom.html', context)

    if request.method == 'POST':
        classroom_id = Classroom.objects.get(id = id)
        data = ClassRegister.objects.filter(user = request.user ,classroom_id = classroom_id)
        if data:
            messages.success(request, 'Already registered in classroom')
            return render(request,'classroom.html', context)
        data = ClassRegister.objects.create(user =request.user, classroom_id = classroom_id)
        messages.success(request, 'registered in classroom successfully')
        return redirect('home')

@login_required
def classroom_details(request):
    classroom_id = ClassRegister.objects.filter(user = request.user.id).values('classroom_id')
    data = []
    for course in classroom_id:
        data.append(course['classroom_id'])
    classroom_details =(Classroom.objects.filter(id__in = data))

    
    context ={
        'classroom_details':classroom_details,
    }
    
    
    if request.method == 'GET':
        return render(request,'classroom_details.html', context)


@staff_member_required
def add_classroom(request):
    
    if request.method =='GET':
        return render(request,'add_classroom.html')

    if request.method == 'POST':
        title = request.POST['title']
        description = request.POST['description']
        
        if not title:
            messages.error(request, 'title is required')
            return render(request,'add_classroom')
        
        if not description:
            messages.error(request, 'description is required')
            return render(request,'add_classroom')
       
        Classroom.objects.create(title= title ,description=description)
        messages.success(request, 'classroom is saved')
        return render(request,'add_classroom.html')

