from django.contrib import admin
from .models import Classroom,ClassRegister

admin.site.register(Classroom)
admin.site.register(ClassRegister)
