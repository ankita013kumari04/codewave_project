from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Classroom(models.Model):
    title = models.CharField(max_length= 250,null = False,blank=False)
    description = models.TextField()

    def __str__(self):
        return self.title
    
class ClassRegister(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE, null=False, blank=False)
    classroom_id = models.ForeignKey(Classroom,on_delete=models.CASCADE, null=False, blank=False)

    def __str__(self):
        return self.user

